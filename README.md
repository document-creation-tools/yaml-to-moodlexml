# Description

## Structure

Questions (see next section) are stored in `.yaml` format.
Multiple questions in one file (separated with `---`) are considered one task (one page in Moodle).
All files (tasks) in a folder are considered one exercise.

Example document tree:
```
exercises
 |
 +----01
 |
 \----02
      |
      +----task01.yaml
      |
      \----task02.yaml
```

## Elements

There are Description and Question elements.
Description are identified by the `task-title` element, instead of the `title` used for questions.

Both support the `files` tag.
Files (path is relative to the `.yaml` you are writing) are linked to in the bottom of the description text.
The files paths are simply given as a list.

### Descriptions

To get an description element (to describe the overall task), you use a `task-title` element instead of the `title` one.

The possible entries for this type are:
- `description` (required, string)

Example:
```yaml
task-title: Test
description: |-
  Lorem ipsum ![text](img.jpg)

files:
  - a02.yaml
```

### Question

This format currently supports four types of questions.
All **require** the keys: `title`, `type`, `points`, and `question`.

The question's text and answers support variables, HTML paragraphs are inserted if a empty line is inserted, otherwise it is not modified.
Hence, you can write HTML and MathJax, or any other format supported by Moodle and the settings on the course.

#### Variables

A variable is surrounded by `{}`. The value is computed (Python eval) though a python expression in the `vars` section of the question's definition.
If such a variable is given different resulting values are presented to the student.
If you want a formula to be evaluated after a random variable is placed within it, you need to append `[e]`.
```yaml
type: direct-answer
points: 5
question: Compute {x} * {x}
answer: "{x} * {x} [e]"
vars:
  x: random.randint(3, 9)
```
In this example, 

1. `x` is evaluated to be a random int.
1. Every occurrence of `{x}` is replaced with the int value.
1. Every string with `[e]` at the end is evaluated.

### Question Types
#### `single-choice` and `multiple-choice`

For single and multiple choice answers.

The possible entries for this type are:
- `answers` (required, list of dicts) where the dict required `true` or `false` as key and optionally `fraction`, whcih overwrites the computed percentage that is given for the specific answer.
  ```yaml
  answers:
    - correct: Answer1
    - correct: Answer2
      fraction: +50
    - incorrect: Answer3
    - incorrect: Answer4
    - incorrect: Answer5
  ```
- `variance` (not required (only for multiple choice), number in percent) 
  in case this is not set all incorrect answers are weighted, so that selecting ALL answers results in 0pt for the student (`relative_weight = 100%/(#incorrect)`).\
  If the `variance` is set, the `relative_weight` is adjusted by the given percentage.

  Example (percentage in braces is added/subtracted from the points if the answer is selected by the student):
  ```
  variance: None              variance: 10              variance: -5
  correct:                    correct:                  correct:
    - Answer 1 (+50%)           - Answer 1 (+50%)         - Answer 1 (+50%)
    - Answer 2 (+50%)           - Answer 2 (+50%)         - Answer 2 (+50%)
  incorrect:                  incorrect:                incorrect:
    - Answer 3 (-25%)           - Answer 3 (-15%)         - Answer 3 (-30%)
    - Answer 4 (-25%)           - Answer 4 (-15%)         - Answer 4 (-30%)
    - Answer 5 (-25%)           - Answer 5 (-15%)         - Answer 5 (-30%)
    - Answer 6 (-25%)           - Answer 6 (-15%)         - Answer 6 (-30%)
  ```
- `shuffle_answers` (not required, boolean, default true)
  determines whether the answers will be shuffled by Moodle.
  If `false` the same order as in the YAML will be displayed.

#### `direct-answer`

For answers that require a direct match (alphabetical).
The possible entries for this type are:
- `answer` (required, any 'simple' type like str, int, float, ... and a list of those) the value
  - e.g.:
    ```
    answer:
      - 42
      - \x2A
    ```
    ```
    answer: '42'
    ```
- `usecase` (not required, bool, default: true) whether to check for equal casing of the answer, or not.

#### `free-answer`

For free text or file-based answers.
The possible entries for this type are:
- `file-submission` (not required, dict) default: `allow=True`
  - `allow` or `require` (exactly one of both is required, bool or int) determines if files are allowed or required.
    `allow=True` is equal to `allow=-1` and implies, that the number of uploaded files is unlimited, non are required.
    `allow=False` is equal to `allow=0` and implies, that no files are allowed to be uploaded.
    `allow=1`, `allow=2`, `allow=3`: number of allowed files.
    
    `require=1`, `require=2`, `require=3`: number of files that are required to be uploaded.
    If both `allow` and `require` are set, `allow` will be ignored.
  - `types` (not required, list[string] or string) all allowed filetypes,
    if none given, but `allow=True` all filetypes are allowed
- `require-text-submission` (not required, bool) determines if the student has to write something in the box, default: False

# Example

`task01.yaml`:
```yaml
---
task-title: Test 2
description: |-
  Lorem ipsum ![text](img.jpg)
---


title: Q1
type: single-choice
points: 10
question: |-
  Lorem ipsum

answers:
  - correct: Answer 1
  - incorrect: Answer 3
  - incorrect: Answer 4
  - incorrect: Answer 5
  - incorrect: Answer 6
  - incorrect: Answer 7 {x}
vars:
  x: random.randint(0, 100)

---

title: Q2
type: multiple-choice
points: 10
variance: -5
question: |-
  Lorem ipsum

answers:
    - correct: Answer1
    - correct: Answer2
      fraction: +50
    - incorrect: Answer3
    - incorrect: Answer4
    - incorrect: |-
        multi line
        answer

---

title: Q3
type: direct-answer
points: 5
question: |-
  Lorem ipsum
files:
  - some_file.pdf

answer: 42
```

`task02.yaml`:
```yaml
---
task-title: Test 2
description: |-
  Lorem ipsum

files:
  - a02.yaml
---

title: Q4
type: free-answer
points: 12
question: |-
  Lorem ipsum

file-submission:
  require: 1
require-text-submission: False
```
