import argparse
import logging
import os
import sys

from yaml_to_moodlexml.yaml_parser import YamlParser

logging.basicConfig(
    stream=sys.stdout,
    level=logging.DEBUG,
    format="%(asctime)s %(levelname)8s | %(name)s: %(message)s",
)
logger = logging.getLogger(__name__)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Convert YAML descriptions to MoodleXML.")
    parser.add_argument("files", metavar="FILE", nargs="+", help="the paths to the YAML files")
    parser.add_argument(
        "--eval-file",
        "-e",
        dest="exec_module_path",
        help="Python file that is imported so its functions "
        "can be used in the YAML's vars section",
    )
    parser.add_argument(
        "--category",
        "-c",
        help="format string to be used as the category for each question. "
        "Possible variables are: `task_title`, `file_name`. "
        "`file_name` is name of the file without any ending "
        "(everything prior to the first '.'). "
        "The default is '$module$/top/{task_title}'",
    )
    parser.add_argument(
        "--output",
        "-o",
        help="location to output the resulting XML file to. " "Defaults to {CWD}/output.xml",
    )

    args = vars(parser.parse_args())
    logger.debug("Args are: %s", args)
    files = [os.path.abspath(p) for p in args["files"]]
    args.pop("files", None)
    if args["output"] is not None:
        output = args["output"]
    else:
        output = os.path.join(os.getcwd(), "output.xml")
    args.pop("output", None)
    print("Output XML to:", output)
    for k, v in dict(args).items():
        if v is None:
            del args[k]

    yaml_parser = YamlParser(**args, task_header_prefix="Aufgabe")

    for file in files:
        print("Process file:", file)
        yaml_parser.parse_file(file)

    yaml_parser.write_xml(output)
