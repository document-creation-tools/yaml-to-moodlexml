import bisect
import copy
import importlib.util
import logging
import os
import re
from html import escape
from typing import List, Union

import yaml
from moodlexml_writer.builder import MoodleXmlBuilder

logger = logging.getLogger(__name__)

# the keys in a question dict, that accept a variable
# (which will be inserted through a format string)
FORMATTABLE = [
    "question",
    "correct",
    "incorrect",
    "answer",
]
# fractions that are allowed by Moodle
FRACTIONS = [
    -100.0,
    -90.0,
    -83.33333,
    -80.0,
    -75.0,
    -70.0,
    -66.66667,
    -60.0,
    -50.0,
    -40.0,
    -33.33333,
    -30.0,
    -25.0,
    -20.0,
    -16.66667,
    -14.28571,
    -12.5,
    -11.11111,
    -10.0,
    -5.0,
    0.0,
    5.0,
    10.0,
    11.11111,
    12.5,
    14.28571,
    16.66667,
    20.0,
    25.0,
    30.0,
    33.33333,
    40.0,
    50.0,
    60.0,
    66.66667,
    70.0,
    75.0,
    80.0,
    83.33333,
    90.0,
    100.0,
]


def _closest_allowed_fraction(_frac):
    """Returns the fraction in FRACTIONS closest to the given number."""
    _rounded = round(_frac, 5)
    if _rounded in FRACTIONS:
        return _rounded
    # gets the index where _frac would be inserted into FRACTIONS
    i = bisect.bisect_left(FRACTIONS, _frac)
    if i == 0:
        return FRACTIONS[0]
    if i == len(FRACTIONS):
        return FRACTIONS[-1]
    left = FRACTIONS[i - 1]
    right = FRACTIONS[i]
    if abs(left - _frac) < abs(right - _frac):
        return left
    else:
        return right


def _insert_paragraphs(string):
    """Wraps text in paragraphs, starts new one where there is an empty line."""
    _content = ["<p>"]
    for line in string.split("\n"):
        if line.strip() == "":
            _content.append("</p>\n<p>")
        else:
            _content.append(line)
    _content.append("</p>")
    return " ".join(_content)


def _handle_code_env(html):
    code_regex = re.compile(r"`(.+?)`", re.DOTALL)
    opening_code = (
        '<code style="color: black;background-color: rgba(0, 0, 0, 0.06);'
        'border-radius: 6px;padding: 0.2em 0.4em;">'
    )
    closing_code = "</code>"

    def process(matchobj):
        return opening_code + escape(matchobj[1]) + closing_code

    return re.sub(code_regex, process, html)


class YamlParser:
    """Add the questions from a YAML task file to the internal state of the MoodleXmlWriter.

    Args:
        xml_builder:
            MoodleXmlBuilder object to write the parsed data to.
        category:
            Format string to be used as the category for each question.
        nr_rand_gen:
            If there is a random variable in a question: how many variations of the question
            should be created?
        exec_module_path:
            Path to python file (that is imported as a module) where functions can be defined which
            can be used in the random vars.
        task_header_prefix:
            Prefix to the task title.
    """

    def __init__(
        self,
        xml_builder: MoodleXmlBuilder = None,
        category: str = "$module$/top/{task_title}",
        nr_rand_gen: int = 5,
        exec_module_path: str = "",
        task_header_prefix: str = "Task",
    ):
        self.builder = xml_builder if xml_builder is not None else MoodleXmlBuilder()
        self.category = category
        self.nr_rand_gen = nr_rand_gen
        self.task_header_prefix = task_header_prefix
        self._category = self.category.format(task_title="", file_name="")
        self._yaml_file_path = ""
        self.task_nr = 0
        self.question_nr = 0
        if exec_module_path:
            # automatically load the python functions specified for eval usage
            module_name = os.path.basename(exec_module_path).split(".")[0]
            spec = importlib.util.spec_from_file_location(module_name, exec_module_path)
            module = importlib.util.module_from_spec(spec)
            spec.loader.exec_module(module)
            globals()[module_name] = module

    def write_xml(self, path: str):
        """Writes the constructed XML file to the specified path."""
        with open(path, "w") as file:
            file.write(self.builder.xml)

    def parse_file(self, file_path: str):
        """Parses the YAML file at the specified path into XML format."""
        self._category = self.category.format(task_title="", file_name="")
        self._yaml_file_path = file_path
        with open(file_path, "r") as file:
            _yaml = yaml.safe_load_all(file.read())
        for element in _yaml:
            if element is None:
                continue
            if "task-title" in element:
                self.task_nr += 1
                self.question_nr = 0
                logger.info("Process description element %s", element)
                self._process_description(file_path, element)
            else:
                self.question_nr += 1
                logger.info("Process question element %s", element)
                self._process_question(element)

    def _process_description(self, file_path: str, element: dict):
        """Processes a description element.

        Currently using a 'task-title' key automatically starts a new task.
        Adding a description element on its own is not supported.
        """
        self._category = self.category.format(
            task_title=element["task-title"], file_name=os.path.basename(file_path).split(".")[0]
        )
        file_paths = self._get_file_paths(element.get("files"))
        header = f"<h2>{self.task_header_prefix} {self.task_nr}: {element['task-title']}</h2>\n"
        description = header + _insert_paragraphs(element["description"])
        self.builder.add_description(
            self._category,
            {
                "title": f"{self.task_nr:02} {element['task-title']}",
                "description": _handle_code_env(description),
                "file_paths": file_paths,
            },
        )

    def _process_question(self, question: dict):
        """Processes a question element."""

        if isinstance(question.get("vars"), dict) and question.get("vars"):  # there is a variable
            self._add_randomized_questions(question)
        else:
            self._add_question(question)

    def _get_file_paths(self, _path_list: List[str]) -> List[str]:
        """Prefix the file list with the location of the YAML file.

        This enables relative file positions from the YAML file, to images, etc.
        """
        _path_base = os.path.dirname(os.path.abspath(self._yaml_file_path))
        if not _path_list:
            return [_path_base]
        return [_path_base] + _path_list

    def _add_randomized_questions(self, question):
        """Eval the formulars defined in the YAML.

        TODO: does not work with the new multiple choice format! REDO!
        """
        _vars = question.get("vars")
        for _ in range(self.nr_rand_gen):  # how many different questions should be generated
            _question_instance = copy.deepcopy(question)
            _vars_instance = {}
            for k, v in _vars.items():  # eval the formular, creates values for the variables
                try:
                    _vars_instance[k] = eval(v)  # pylint: disable=eval-used
                except Exception as e:  # pylint: disable=broad-except
                    _vars_instance[k] = "<EVAL FAILED>"
                    logger.warning("Error during evaluation of formula: %s: %s. %s", k, v, e)
            for key in FORMATTABLE:  # check every key that is allowed to contain a formular
                if key in _question_instance:
                    # format the strings with the evaluated values
                    if isinstance(_question_instance[key], list):
                        _question_instance[key] = [
                            v.format(**_vars_instance) for v in _question_instance[key]
                        ]
                    else:
                        _question_instance[key] = _question_instance[key].format(**_vars_instance)

                    # if [e] are the last 3 chars, evaluate the string again, after the values are replaced
                    if _question_instance[key][-3:] == "[e]":
                        try:
                            _question_instance[key] = eval(
                                _question_instance[key]
                            )  # pylint: disable=eval-used
                        except Exception as e:  # pylint: disable=broad-except
                            logger.warning(
                                "Error during evaluation of %s formula in %s: %s",
                                _question_instance[key],
                                _question_instance["title"],
                                e,
                            )
            del _question_instance["vars"]
            self._add_question(_question_instance, subcategory=_question_instance["title"])

    def _add_question(self, question: dict, subcategory: str = ""):
        """Add a question to the XML, set defaults based on its type, etc.

        For the case, that a question contains a random variable you can add it in a
        subcategory in moodle.

        TODO: add flag for optional HTML escaping in answers.
        """

        def _get_iterable(item: Union[str, List[str]] = None) -> List[str]:
            if isinstance(item, list):
                return item
            else:
                return [str(item)]

        _type = ""

        if question["type"] == "single-choice" or question["type"] == "multiple-choice":
            _type = "multichoice"
            if "shuffle_answers" not in question:
                question["shuffle_answers"] = True

            _correct = 0
            _incorrect = 0
            for answer in question["answers"]:
                if "correct" in answer:
                    _correct += 1
                elif "incorrect" in answer:
                    _incorrect += 1
                else:
                    raise ValueError(f"Answers requires correct or incorrect key: {answer}")

            if question["type"] == "single-choice":
                question["single_choice"] = True
                _fraction_correct = 100
                _fraction_incorrect = 0
            else:
                question["single_choice"] = False
                if "variance" in question:
                    _variance = question["variance"] / 100
                else:
                    _variance = 0
                if _correct > 0:
                    _fraction_correct = _closest_allowed_fraction(100 / _correct)
                if _incorrect > 0:
                    _fraction_incorrect = _closest_allowed_fraction(-100 / _incorrect + _variance)

            _new_answers = []
            for answer in question["answers"]:
                if "correct" in answer:
                    _frac = answer.get("fraction")
                    if _frac is None:
                        _frac = _fraction_correct
                    _new_answers.append(
                        {
                            "text": escape(str(answer["correct"])),
                            "fraction": _frac,
                        }
                    )
                elif "incorrect" in answer:
                    _frac = answer.get("fraction")
                    if _frac is None:
                        _frac = _fraction_incorrect
                    _new_answers.append(
                        {
                            "text": escape(str(answer["incorrect"])),
                            "fraction": _frac,
                        }
                    )
            question["answers"] = _new_answers

        elif question["type"] == "direct-answer":
            _type = "shortanswer"
            question["answers"] = []
            for answer in _get_iterable(question["answer"]):
                question["answers"].append(
                    {
                        "text": answer,
                        "fraction": 100,
                    }
                )

        elif question["type"] == "free-answer":
            _type = "essay"
            question["responseformat"] = "plain"
            question["responsetemplate"] = "Upload file below"
            question["responsefieldlines"] = 5
            if "file-submission" not in question:
                question["attachments"] = -1
                question["attachmentsrequired"] = 0
            else:
                if "allow" in question["file-submission"]:
                    _allow = question["file-submission"]["allow"]
                    if isinstance(_allow, bool):
                        if _allow:
                            question["attachments"] = -1
                        else:
                            question["attachments"] = 0
                    else:
                        question["attachments"] = _allow
                    question["attachmentsrequired"] = 0
                if "require" in question["file-submission"]:
                    question["attachments"] = question["file-submission"]["require"]
                    question["attachmentsrequired"] = question["file-submission"]["require"]

            if "require-text-submission" in question:
                question["responserequired"] = question["require-text-submission"]
                if question["require-text-submission"]:
                    question["responseformat"] = "editor"
                    question["responsetemplate"] = ""
                    question["responsefieldlines"] = 20

        else:
            raise ValueError(f"Unsupported type: {question['type']}")

        question["file_paths"] = self._get_file_paths(question.get("files"))

        # add the question title in HTML and count the subtasks in a) b) c) ...
        assert self.question_nr <= 26
        question_text = (
            f"<p><b>{self.task_nr}{chr(96+self.question_nr)})</b> {question['title']}</p>\n"
            + _insert_paragraphs(question["question"])
        )
        question["question"] = _handle_code_env(question_text)

        # number title so it can be sorted and imported in the correct order in moodle
        question["title"] = f"{self.task_nr:02}.{self.question_nr:02} {question['title']}"

        # pop keys from yaml that would cause errors on answer object
        for k in [
            "correct",
            "variance",
            "incorrect",
            "answer",
            "require-text-submission",
            "file-submission",
            "files",
            "type",
        ]:
            question.pop(k, None)

        _category = self._category if not subcategory else f"{self._category}/{subcategory}"
        self.builder.add_entry(_type, _category, question)
